value=str(input("Enter The Value:"))
# a).extract from input => "one"

txt=value[-4:-1]
print("This is we are expecting : ",txt)

# b).convert (a) from "one" to => "One"
txt1=value.replace("one","One")
print("This Is The Result After Converting :",txt1)

# c). reverse the input => "senorhTfOemaG"

rev=value[::-1]
print("After Reversing The String Value: ",rev)

# d).  reverse the input case => "gAMEoFtHRONES"

rev_case=value.swapcase()
print("After swapcase the String Case :",rev_case)

# e) extract caps into one string => "GOT"
ext=''.join(c for c in value if c.isupper())
print("After Extract The String: ",ext)